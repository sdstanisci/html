var titulo = document.querySelector(".titulo");
titulo.textContent = "Teste";

var pacientes = document.querySelectorAll(".paciente");
//console.log(pacientes);

for(var i=0; i<pacientes.length; i++){
	
	var paciente = pacientes[i];

	var tdPeso = paciente.querySelector(".info-peso");
	var peso = tdPeso.textContent;
	
	var tdAltura = paciente.querySelector(".info-altura");
	var altura = tdAltura.textContent;
		
	var tdImc = paciente.querySelector(".info-imc"); 
	
	var pesoEhValido = validaPeso(peso);
	var alturaEhValida = validaAltura(altura);
	
	
	
	if(!pesoEhValido){
	    //console.log("Peso inválido");
	    tdImc.textContent = "Peso inválido!";
	    paciente.classList.add("paciente-invalido");
	    pesoEhValido = false;
	}
	

	if(!alturaEhValida){
	    //console.log("Altura inválida");
	    tdImc.textContent = "Altura inválida!";
	    paciente.classList.add("paciente-invalido");	
	    alturaEhValida = false;
	} 

	if(pesoEhValido && alturaEhValida){
	    var imc = calculaImc(peso, altura);    
	    tdImc.textContent = imc;
	}
	

}

/*titulo.addEventListener("click",function(){
	console.log("Teste");
});*/

function validaPeso(peso){
	
	if(peso <= 0 || peso > 1000){
	    return false;
	}
	return true;
}

function validaAltura(altura){

	if(altura <= 0 || altura >= 10.00){
	   return false;
	} 
	return true;
}

function calculaImc(peso, altura){
	
	console.log("peso:"+peso);
	console.log("altura:"+altura);
	
	var imc = 0;
	imc = peso / ( altura * altura);
	
	console.log(imc);
	return imc.toFixed(2);
}