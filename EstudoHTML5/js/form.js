var botaoAdicionar = document.querySelector("#adicionar-paciente");
//console.log(botaoAdicionar);
botaoAdicionar.addEventListener("click",function(){
	//prevenir o comportamento padrao do form, nao eh feito o submit no form
	event.preventDefault();
	//console.log("Teste");
	
	var form = document.querySelector("#form-adiciona");
	var paciente = obtemPacienteDoFormulario(form);
	
	var erros = validaPaciente(paciente);
	
	if(erros.length > 0){
			
		exibeMensagensDeErro(erros);
		
		// se fosse para exibir dentro de um span vazio
		//var mensagemErro = document.querySelector("#mensagem-erro");
		//mensagemErro.textContent = erro;
		return;
	}
	
	//console.log("Paciente:" + paciente);
	//console.log("Nome" + paciente.nome);

	adicionaPacienteNaTabela(paciente);
	
	form.reset();
	var mensagensErro = document.querySelector("#mensagens-erro");
	mensagensErro.innerHTML = "";
			
});

function adicionaPacienteNaTabela(paciente){
	
	var pacienteTr = montaTr(paciente);
	var tabela = document.querySelector("#tabela-pacientes");
	tabela.appendChild(pacienteTr);
}


function obtemPacienteDoFormulario(form){
	
	var paciente = {
			nome : form.nome.value,
			peso : form.peso.value,
			altura : form.altura.value,
			gordura : form.gordura.value,
			imc : calculaImc(form.peso.value, form.altura.value)
	}
	return paciente;
}

function montaTr(paciente){
	var tr = document.createElement("tr");
	tr.classList.add("paciente");
	
	tr.appendChild(montaTd(paciente.nome, "info-nome"));
	tr.appendChild(montaTd(paciente.peso, "info-peso"));
	tr.appendChild(montaTd(paciente.altura, "info-altura"));
	tr.appendChild(montaTd(paciente.gordura, "info-gordura"));
	tr.appendChild(montaTd(paciente.imc, "info-imc"));
		
	return tr;
}

function montaTd(dado, classe){
	
	var td = document.createElement("td");
	td.textContent = dado;
	td.classList.add(classe);
	return td;
}

function validaPaciente(paciente){
	
	// arraay
	var erros = [];
	
	//push add no array
	if(paciente.nome.length == 0) erros.push("Nome vazio!");
	if(!validaPeso(paciente.peso)) erros.push("O peso eh Invalido!");
	if(!validaAltura(paciente.altura)) erros.push("A altura eh Invalida!");
	if(paciente.gordura.length == 0) erros.push("Gordura não pode ser em branco!");
		
	return erros;
}

function exibeMensagensDeErro(erros){
	
	var ul = document.querySelector("#mensagens-erro");
	
	/*for(var i=0; i<erros.length; i++){
		var erro = erros[i];
	}*/
	
	ul.innerHTML = "";
	
	erros.forEach(function(erro){
		var li = document.createElement("li");
		li.textContent = erro;
		ul.appendChild(li);
	});
	
}

